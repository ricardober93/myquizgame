import { Button, Center, Heading, Input, VStack } from "@chakra-ui/react";
import { useState } from "react";
import useAuth from "../Hooks/useAuth";
import { useAuthContext } from "./../context/AuthContext";

export const Auth = () => {
  const [name, setName] = useState("");
  const { setUser } = useAuthContext();
  const { login } = useAuth();

  const handleLogin = () => {
    setUser(name);
    login();
  };
  return (
    <Center minH="100vh" background="purple.500">
      <VStack bg={"white"} borderRadius={12} p={5}>
        <Heading textAlign="center" size="lg">
          ¿Cómo te llamas?
        </Heading>
        <Input onChange={(e) => setName(e.target.value)} />

        <Button onClick={() => handleLogin()} colorScheme="messenger">
          Jugar
        </Button>
      </VStack>
    </Center>
  );
};
