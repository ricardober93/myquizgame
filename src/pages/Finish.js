import { Center, Button, VStack, Heading, Text } from "@chakra-ui/react";
import useQuiz from "../Hooks/useQuiz";
import { useAuthContext } from "../context/AuthContext";
import useQuestion from "../Hooks/useQuestion";

const Finished = () => {
  const { restart, counter, isAllCorrect } = useQuiz();
  const { user } = useAuthContext();
  const { goToNewQuestionPage  } = useQuestion()

  return (
    <Center minH="100vh" background="purple.500">
      <VStack bg={"white"} borderRadius={12} p={5}>
        <Heading textAlign="center" size="lg">
          Felicitaciones {user}
        </Heading>
        <Text>
          {" "}
          Obtuviste{" "}
          <Text
            as="span"
            color={` ${counter < 1 ? "red" : "green"}`}
            fontSize="2xl"
          >
            {counter}
          </Text>{" "}
          respuestas correctas.{" "}
        </Text>
        <Button onClick={restart} colorScheme="messenger">
          Intentar de nuevo
        </Button>
        {isAllCorrect() ? (
          <Button onClick={goToNewQuestionPage} colorScheme="teal">
            Nueva Pregunta
          </Button>
        ) : null}
      </VStack>
    </Center>
  );
};

export default Finished;
