import { Box, Heading, VStack, Button } from "@chakra-ui/react";
import ButtonQuiz from "../components/ButtonQuiz";
import useQuiz from "../Hooks/useQuiz";

const Quiz = () => {
  const { currentQuestion, changeQuestion, show } = useQuiz();
  return (
    <VStack
      minH="100vh"
      background="purple.500"
      display="flex"
      justifyContent={"center"}
      alignItems="center"
    >
      <Heading  textAlign="left" color="white">
        DEV Quiz
      </Heading>
      <Box  width={"85%"} bg={"white"} borderRadius={12} p={5}>
        {
          <>
            <Heading textAlign="left" size="lg">
              {currentQuestion?.question}
            </Heading>
            <VStack mt={5}>
              {currentQuestion?.answers.map((ans) => (
                <ButtonQuiz
                  key={ans.id}
                  ans={ans}
                  answerCorrect={ans?.correct}
                  show={show}
                />
              ))}
            </VStack>
            {show === true ? (
              <Box width="100%" mt={3} display="flex" justifyContent="end">
                <Button onClick={changeQuestion} colorScheme="yellow">
                  {" "}
                  siguiente{" "}
                </Button>
              </Box>
            ) : null}
          </>
        }
      </Box>
    </VStack>
  );
};
export default Quiz;
