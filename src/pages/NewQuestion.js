import { Center, Heading, VStack } from "@chakra-ui/react";
import FormNewQuiz from './../components/FormNewQuiz'

export const NewQuestion = () => {

  return (
    <Center minH="100vh" background="purple.500">
      <VStack bg={"white"} borderRadius={12} p={5}>
        <Heading textAlign="center" size="lg">
          Agrega una nueva Pregunta
        </Heading>
      <FormNewQuiz />
      </VStack>
    </Center>
  );
};