import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { fecther } from "../Api/fetcher";
import { url } from "../config/Constant";
import useQuiz from "./useQuiz";

function useQuestion() {
  let navigate = useNavigate();
  const {clearData} =  useQuiz();
  const [question, setQuestion] = useState("");
  const [optionA, setOptionA] = useState();
  const [optionB, setOptionB] = useState();
  const [optionC, setOptionC] = useState();
  const [optionD, setOptionD] = useState();
  const [answers, setAnswer] = useState([]);
  const [canSubmit, setCanSubmit] = useState(false);

  const goToNewQuestionPage = () => {
    navigate(`/newQuestion`, { replace: true });
  };

  const onChangeQuestion = (e) => {
    const question = e.target.value;
    setQuestion(question);
  };

  const onChangeAnswerA = (e) => {
    const answer = {
      option: e.target.name,
      answer: e.target.value,
      correct: false,
    };
    setOptionA(answer);
  };

  const onChangeAnswerB = (e) => {
    const answer = {
      option: e.target.name,
      answer: e.target.value,
      correct: false,
    };
    setOptionB(answer);
  };

  const onChangeAnswerC = (e) => {
    const answer = {
      option: e.target.name,
      answer: e.target.value,
      correct: false,
    };
    setOptionC(answer);
  };

  const onChangeAnswerD = (e) => {
    const answer = {
      option: e.target.name,
      answer: e.target.value,
      correct: false,
    };
    setOptionD(answer);
  };

  const onChangeSelectCorrect = (e) => {
    const correct = e.target.value;
    const newAnswerCorrect = answers.map((a, i, array) => {
      const index = array.findIndex((a) => a.option === correct);
      console.log(index);
      a.correct = false;
      array[index].correct = true;
      return { ...a };
    });
    setCanSubmit(true);
    setAnswer(newAnswerCorrect);
  };

  const save = (e) => {
    e.preventDefault();
    setAnswer([optionA, optionB, optionC, optionD]);
  };

  const saveQuestion = (data) => {
    fecther(url + "/save", {
      method: "post",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data) ,
    }).then((res) => {
      clearData()
      navigate(`/`, { replace: true })});
  };

  const onSubmit = (e) => {
    e.preventDefault();

    const data = {
      question: "¿" + question + "?",
      answers,
    };
    saveQuestion(data);
  };

  return {
    goToNewQuestionPage,
    onChangeQuestion,
    onChangeAnswerA,
    onChangeAnswerB,
    onChangeAnswerC,
    onChangeAnswerD,
    onChangeSelectCorrect,
    save,
    canSubmit,
    onSubmit,
  };
}
export default useQuestion;
