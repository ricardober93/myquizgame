import { useNavigate } from "react-router-dom";

function useAuth() {
  const navigate = useNavigate();

  const login = () => {
    navigate("/quiz", { replace: true });
  };

  return {
    login,
  };
}
export default useAuth;
