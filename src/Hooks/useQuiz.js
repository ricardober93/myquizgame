import { useState, useEffect, useCallback } from "react";
import { useNavigate } from "react-router-dom";
import { useRecoilState } from "recoil";
import { fecther } from "../Api/fetcher";
import { url } from "../config/Constant";
import { counterState, showState } from "../store/atoms";

function useQuiz() {
  let navigate = useNavigate();
  const [currentIndex, setCurrentIndex] = useState(0);
  const [show, setShow] = useRecoilState(showState);
  const [counter, setCounter] = useRecoilState(counterState);
  const [quiz, setQuiz] = useState([]);


  const clearData = () => {
    setCounter(0)
  }

  const quizLoad = () => {
    fecther(url, { method: "get" }).then((res) => setQuiz(res));
  };

  const callBack = useCallback(quizLoad, []);

  useEffect(() => {
    callBack();
  }, [callBack]);

  const currentQuestion = quiz[currentIndex];

  const nextQuestion = () => {
    if (currentIndex < quiz.length - 1) {
      setCurrentIndex(currentIndex + 1);
    }
    if (currentIndex === quiz.length - 1) {
      navigate(`/finish`, { replace: true });
    }
  };
  const restart = () => {
    navigate(`/`, { replace: true });
    setCounter(0);
  };

  const changeQuestion = () => {
    setShow(false);
    nextQuestion();
  };

  const toogleShow = () => {
    setShow(true);
  };

  const counterPlus = () => {
    setCounter((counter) => counter + 1);
  };

  const isAllCorrect = () => {
    return counter === quiz.length
  }

  return {
    currentQuestion,
    currentIndex,
    nextQuestion,
    restart,
    changeQuestion,
    show,
    counter,
    toogleShow,
    counterPlus,
    isAllCorrect,
    clearData
  };
}
export default useQuiz;
