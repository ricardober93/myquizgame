import { atom } from 'recoil';

export const showState = atom({
    key: 'showState', // unique ID (with respect to other atoms/selectors)
    default: false, // default value (aka initial value)
  });
  
  export const counterState = atom({
    key: 'counterState', // unique ID (with respect to other atoms/selectors)
    default: 0, // default value (aka initial value)
  });
  