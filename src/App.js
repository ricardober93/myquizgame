import { ChakraProvider } from "@chakra-ui/react";
import { Route, Routes } from "react-router-dom";
import { RecoilRoot } from "recoil";
import "./App.css";
import ProtectedRoute from "./components/ProtectedRoute";
import { AuthProvider } from "./context/AuthContext";
import { Auth } from "./pages/Auth";
import Finished from "./pages/Finish";
import { NewQuestion } from "./pages/NewQuestion";
import Quiz from "./pages/Quiz";

function App() {
  return (
    <RecoilRoot>
      <ChakraProvider>
        <AuthProvider>
          <Routes>
            <Route path="/" element={<Auth />} />
            <Route
              path="/quiz"
              element={
                <ProtectedRoute>
                  <Quiz />
                </ProtectedRoute>
              }
            />
             <Route
              path="/newQuestion"
              element={
                <ProtectedRoute>
                  <NewQuestion />
                </ProtectedRoute>
              }
            />
            <Route path="finish" element={<Finished />} />
          </Routes>
        </AuthProvider>
      </ChakraProvider>
    </RecoilRoot>
  );
}

export default App;
