import { Navigate, Outlet } from "react-router-dom";
import { useAuthContext } from "./../context/AuthContext";
const ProtectedRoute = ({ children }) => {
  const {user} = useAuthContext();

  if (user === "") {
    return <Navigate to="/" replace />;
  }

  return children ? children : <Outlet />;
};

export default ProtectedRoute;
