import {
  FormControl,
  FormLabel,
  Input,
  VStack,
  HStack,
  Divider,
  Button,
  Select,
} from "@chakra-ui/react";
import useQuestion from "../Hooks/useQuestion";

const FormNewQuiz = () => {

  const { onChangeQuestion,
    onChangeAnswerA,
    onChangeAnswerB,
    onChangeAnswerC,
    onChangeAnswerD,
    onChangeSelectCorrect,
    canSubmit,
    save,
    onSubmit } = useQuestion()
  return (
    <VStack p={2}>
      <FormControl>
        <FormLabel htmlFor="email">Pregunta</FormLabel>
        <Input onChange={onChangeQuestion} name="question" type="text" />
      </FormControl>

      <Divider />

      <HStack p={2}>

        <FormControl>
          <FormLabel >Respuesta 1</FormLabel>
          <Input name="A" onChange={onChangeAnswerA} type="text" />
        </FormControl>

        <FormControl>
          <FormLabel >Respuesta 2</FormLabel>
          <Input name="B" onChange={onChangeAnswerB} type="text" />
        </FormControl>

      </HStack>

      <HStack p={2}>
        <FormControl>
          <FormLabel >Respuesta 3</FormLabel>
          <Input name="C" onChange={onChangeAnswerC} type="text" />
        </FormControl>

        <FormControl>
          <FormLabel >Respuesta 4</FormLabel>
          <Input name="D" onChange={onChangeAnswerD} type="text" />
        </FormControl>
      </HStack>
      <Button type="button" onClick={save} colorScheme='green'>Guardar Borrador</Button>
      <Divider />

      <FormControl>
        <Select onChange={onChangeSelectCorrect} placeholder='Respuesta Correcta'>
          <option value='A'>Option A</option>
          <option value='B'>Option B</option>
          <option value='C'>Option C</option>
          <option value='D'>Option D</option>
        </Select>
      </FormControl>



      <Button disabled={!canSubmit} type="button" onClick={onSubmit} colorScheme='blue'>Enviar</Button>
    </VStack >
  );
};
export default FormNewQuiz;
