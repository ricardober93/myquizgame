import { Button, Text } from "@chakra-ui/react";
import { useState } from "react";
import useQuiz from "../Hooks/useQuiz";

const ButtonQuiz = ({ ans, answerCorrect, show }) => {
  const { toogleShow, counterPlus } = useQuiz();
  const [color, setColor] = useState("gray");
  const [variant, setVariant] = useState("outline");

  const confirmAnswer = () => {
    if (answerCorrect) {
      setColor("green");
      setVariant("solid");
      toogleShow();
      counterPlus();
    } else {
      setColor("red");
      setVariant("solid");
      toogleShow();
    }
  };
  return (
    <Button
      width="100%"
      isDisabled={show}
      colorScheme={color}
      variant={variant}
      onClick={() => confirmAnswer()}
      _hover={{
        background: "yellow.300",
      }}
      wordBreak="break-word"
      whiteSpace={"break-spaces"}
      height="auto"
      p={2}
      textAlign={"left"}
    >
      <Text align="left" width={"100%"} >
        {`${ans.option}. ${" "} ${ans.answer}`}
      </Text>
    </Button>
  );
};
export default ButtonQuiz;
